var addEvent = function(name, namePassword) {
    document.getElementById(name).addEventListener("click", function() {
        let input = document.getElementById(namePassword);
        if (input.type === "password") {
            document.getElementById(name).classList.replace("fa-eye", "fa-eye-slash");
            input.type = "text";
        } else {
            input.type = "password";
            document.getElementById(name).classList.replace("fa-eye-slash", "fa-eye");
        }
    });
};

addEvent("firstInput", "firstPassword");
addEvent("secondInput", "secondPassword");

document.getElementById("btn").addEventListener("click", (event) => {
    if (firstPassword.value === secondPassword.value) {
        errorMessage.innerText = "";
        alert("You are welcome");
    } else {
        errorMessage.innerText = "Нужно ввести одинаковые значения";
    }
    event.preventDefault();
});