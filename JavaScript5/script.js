function createNewUser() {
    let newUser = {
        firstName: prompt("Write your first name: "),
        secondName: prompt("Write your second name: "),
        userDate: new Date(
            prompt("Write your birthday dd.mm.yy: ").split(".").reverse().join(", ")
        ),
        getLogin: function() {
            return this.firstName[0].toLowerCase() + this.secondName.toLowerCase();
        },
        getAge: function() {
            let curentDate = new Date();
            let age =
                this.userDate.getMonth() <= curentDate.getMonth() ?
                curentDate.getFullYear() - this.userDate.getFullYear() :
                curentDate.getFullYear() - this.userDate.getFullYear() - 1;
            return age;
        },
        getPassword: function() {
            return (
                this.firstName[0].toUpperCase() +
                this.secondName.toLowerCase() +
                this.userDate.getFullYear()
            );
        },
    };

    return newUser;
}

let user = createNewUser();

console.log(user.getAge());
console.log(user.getPassword());