function filterBy(list, dataType) {
    return list.filter(function(item) {
        return typeof item !== dataType;
    });
}
document.getElementById("h2").innerText =
    "Result: " + filterBy(["hello", "world", 23, "23", null], "string");

let filter = filterBy(["hello", "world", 23, "23", null], "string");

console.log(filter);